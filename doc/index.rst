###########
Association
###########

This module handles Memberships in Associations,
like clubs, unions or learning societies.

A membership is typically bound to a person
or to an entity (another organization).
Thus it can not be transferred to another party.
And of course,
a Party can be a member of an Association only once at a time.

Being a Member of an Association will give the Member
rights within the Association, esp. the right to vote,
use the Association's facilities
and to participate in the Association.
Members might be obliged to pay fees
or spend some hours of work for the Association.

This module is not about managing services with recurring fees,
like fees for the gym or a social network.
These are often called "membership", too,
but are actually subscriptions
(and "members" are actually customers).


Concepts
################

A Party becomes a Member after approval only.
This module splits approval into two steps:
admission and activating as a member.
This is done to cover cases where the member-to-be,
after being admitted to become a member,
has to provide something to actually become a member
(e.g. pay some entry fee, pay cooperative shares).

Any party can be a member of the same association only once at a time.
The same party might apply for another membership later, of course.

While being a Member, the Party can have several "Memberships",
e.g. the "Main Membership" and the membership in the "Woodworking
Special Interest Group".
Individual Memberships can have a starting and ending date,
describing when this Membership applies to the member.
For these Memberships, different Fees can apply.

To easily check if the member paid all its fees,
you can use the "Fees" relate utility.


Membership Types
******************************

Membership Types is how membership fees
and Membership in e.g. a Special Interest Groups (SIG)
are managed.
For example an Association might have different Membership Types
for children (half fee), adults (full fee) and honorary member (no fee),
and membership in the "Woodworking Special Interest Group"
causes additional fees.

Each Membership Type has a list of Periods with Fees.
Of course, Fees can differ between Periods,
and the amount of the Fee can be zero (if no Fee applies).
Periods don’t need to be contiguous.

This information is used by the "Create Fees" Wizard to filter which
Period the Member should be billed.


.. figure:: association.png
   :width: 70%
   :align: center
   :alt: Relation of Membership and Membership Types

   Relation of Membership and Membership Types


Termination
******************************

* If a member wants to terminate its membership,
  enter the desired "Leave Date".
  This module does not enforce any termination dates.
  Thus terminating is allowed at any date,
  not just to the end of periods or fiscal years.

* If the Association requires a period for giving notice of termination,
  the date of the notice of termination can be documented
  in the "Notice of Termination Date".

  If "Time for giving Notice" is configured,
  the "Notice of Termination Date" is required.

* If "Time for giving Notice" is configured,
  and the "Leave Date" is to early
  (relative to the "Notice of Termination Date")
  Tryton will issue a warning when saving the record.

  Tryton does not enforce the "Leave Date" to match the "Time for
  giving Notice", since there may be reasons to terminate the
  Membership earlier, e.g. special termination.

Implementation details:

* The fields "Notice of Termination Date" and "Leave Date"
  are only visible after membership has been admitted
  (state: `running`).

* If a "Notice of Termination Date" is entered,
  a "Leave Date" must be entered, too.


.. figure:: member-states.png
   :alt: The states of a membership life-cycle.


Admonishment & Expulsion
******************************

.. note::
   This is not yet implemented

The Association's statues (or law) might
allow to expulse a member after it has been admonished.
Reasons could be overdue fees or violations of the statues.
Depending on the statues (and law),
admonished members might not have the full rights of a member.
For these cases, member can get the state "admonishend".

If reasons for admonishment are eliminated, the Member can be set back
to "running" state.
Otherwise it can be "expulsed",
which actually terminates its membership.



Configuration
####################

Membership Configurations
******************************

:Member Sequence: Used to generate the number given to association
                  members.
		  Leave empty to assign the number manually.

:Time for giving Notice: Period of time a member has to give notice to
			 terminate membership. (Optional)


Membership Types
******************************

A Membership Type defines a list of periodical fees and accounting
preferences.

A Membership Type is defined by:

:Name:            The name of the Membership Type.
:Journal:         The Journal that will be used for posting accounting move.
:Account Revenue: The account revenue that will be used
		  for posting accounting move.
:Maturity Delay:  The period of time after the membership fees must be
		  paid.
		  FIXME: What unit is used here?
:Periods:         A list of Periods for this Membership Type.


Membership Periods
-------------------

A Period is a range of dates where a fee amount is settled.

A period is defined by:

:Name:        The name of the Period.
:Start Date:  The starting date of the period.
:End Date:    The ending date of the period.
:Amount:      The fee amount for this period.


Implementation
#######################

Member
******

A member is defined by:

:Code:        Main identifier for the member.
:Party:       The party associated with this member.
:Join Date:   The date when the member joins the association.
:Notice of Termination Date: The date when the member gave notice to
			     terminate membership.
:Leave Date:  The date when the member leaves the association.
:Memberships: Manage memberships of the member.


Membership
----------

Each line in the member form is used to specify the starting and ending date
of each membership, defined in the Membership Type.

A membership is defined by:

:Membership Type: The membership the member enrolled.
:Start Date: The starting date of the membership,
	     defaults to the Join Date of the member.
:End Date:   The ending date of the membership,
	     defaults to the Leave Date of the member.

This is used by the "Create Fees" Wizard to filter which period
the user should be billed.


Fees
*******************

A fee is the payment of a member for a specific membership period.

A fee is defined as:

:Member:  The member associated with this fee.
:Period:  The period associated with this fee.
:Move:    The move associated with this fee.
:Paid:    The payment state of the fee.



Wizards
####################


Print Member's Book
*******************

The Member's Book prints a report for all members.


Create Fees Wizard
******************

The wizard will generate Fee records based on the date.
This wizard is used by the cronjob that will trigger (by default)
once every day.


