# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval
from trytond.pool import Pool

from trytond.modules.company.model import (CompanyMultiValueMixin,
    CompanyValueMixin)

__all__ = ['Configuration', 'MemberSequence']


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    """Association Configuration"""
    __name__ = 'association.configuration'
    member_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', "Member Sequence",
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('code', '=', 'association.member'),
            ],
            help=("Used to generate the number given to association members. "
                  "Leave empty to assign the number manually.")))
    termination_notice_period = fields.TimeDelta(
        "Notice Period",
        help=("The minimum period of time between the receipt "
              "of the notice of termination and the end of the membership."))

    @classmethod
    def default_member_sequence(cls, **pattern):
        return getattr(
            cls.multivalue_model('member_sequence'),
            'default_member_sequence', lambda: None)()


class MemberSequence(ModelSQL, CompanyValueMixin):
    """Member Configuration Sequence"""
    __name__ = 'association.configuration.member_sequence'
    member_sequence = fields.Many2One('ir.sequence', "Member Sequence",
        domain=[
            ('company', 'in',
            [Eval('company', -1), None]),
            ('code', '=', 'association.member'),
        ],
        depends=['company'])

    @classmethod
    def default_member_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('association', 'sequence_member')
        except KeyError:
            return None
