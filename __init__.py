# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import association
from . import configuration
from . import ir

__all__ = ['register']


def register():
    Pool.register(
        account.Move,
        association.Member,
        association.PrintMembersBookStart,
        association.MembershipType,
        association.Period,
        association.Fee,
        association.Membership,
        association.GenerateFeeStart,
        configuration.Configuration,
        configuration.MemberSequence,
        ir.Cron,
        module='association', type_='model')
    Pool.register(
        association.MembersBookWizard,
        association.PostFee,
        association.GenerateFee,
        module='association', type_='wizard')
    Pool.register(
        association.MembersBookReport,
        module='association', type_='report')
