# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import timedelta
import hashlib
from sql.operators import Equal
from trytond.config import config
from trytond.i18n import gettext
from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique, \
    Exclude
from trytond.model.exceptions import AccessError
from trytond.pool import Pool
from trytond.pyson import Eval, If, Bool, And
from trytond.report import Report
from trytond.tools import lstrip_wildcard
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateView, Button, StateReport,
    StateTransition)

from .exceptions import (PeriodDateOverlapError, AmountLessThanZeroError,
    PartyAccountPayableRequiredError, MemberUnpaidFeeError,
    NoticePeriodNotObservedWarning)


__all__ = [
    'Member', 'MembersBookWizard', 'PrintMembersBookStart',
    'MembersBookReport', 'Membership', 'Period', 'Fee', 'MembershipType',
    'PostFee', 'GenerateFee', 'GenerateFeeStart'
]

fee_digit = (16, config.getint('membership', 'fee_decimal', default=4))
fee_amount = fields.Numeric("Fee Amount", fee_digit, required=True)

_MEMBER_STATES = [
    ('draft', "Draft"),
    ('admitted', "Admitted"),
    ('running', "Running"),
    ('stopped', "Stopped"),
]

_MOVE_STATES = [
    ('draft', "Draft"),
    ('posted', "Posted"),
]


class Member(Workflow, ModelSQL, ModelView):
    'Member'
    __name__ = "association.member"

    _depends = ['state']

    state = fields.Selection(_MEMBER_STATES, "State", readonly=True)
    code = fields.Char("Code", select=True,
        states={
            'readonly': Eval('code_readonly', True),
            'required': Eval('state') != 'draft'
            },
        depends=_depends + ['code_readonly'],
        help="The internal identifier for the associate.")
    code_readonly = fields.Function(fields.Boolean("Code Readonly"),
                                    'get_code_readonly')
    company = fields.Many2One('company.company', "Company",
        states={'readonly': Eval('state') != 'draft'},
        depends=_depends,
        required=True,
        help="The association the member belongs to.")
    party = fields.Many2One('party.party', "Party",
        states={'readonly': Eval('state') != 'draft'},
        depends=_depends,
        required=True,
        help="The party that represents the member.")

    join_date = fields.Date("Join Date",
        states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state') != 'draft',
        },
        domain=[
            If(Eval('join_date') & Eval('termination_notice_date'),
               ('join_date', '<=', Eval('termination_notice_date')),
               (),
            ),
            If(Eval('join_date') & Eval('leave_date'),
               ('join_date', '<=', Eval('leave_date')),
               (),
            )
        ],
        depends=_depends + ['termination_notice_date', 'leave_date'],
        help="The date the member joined the association.")

    termination_notice_date = fields.Date("Notice of Termination Date",
        domain=[
            If(Eval('termination_notice_date') & Eval('join_date'),
               ('termination_notice_date', '>=', Eval('join_date')),
               (),
            ),
            If(Eval('termination_notice_date') & Eval('leave_date'),
               ('termination_notice_date', '<=', Eval('leave_date')),
               (),
            )
        ],
        depends=_depends + ['join_date', 'leave_date',
                            'termination_notice_period'],
        states={
            'invisible': Eval('state') == 'draft',
            'required': Eval('termination_notice_period') & Eval('leave_date'),
            'readonly': Eval('state') == 'stopped',
        },
        help="The date the member has sent the notice of termination.")
    termination_notice_period = fields.Function(
        fields.TimeDelta("Notice Period"), 'get_termination_notice_period')

    leave_date = fields.Date("Leave Date",
        domain=[
            If(Eval('join_date') & Eval('leave_date'),
               ('leave_date', '>=', Eval('join_date')),
               (),
            ),
            If(Eval('leave_date') & Eval('termination_notice_date'),
               ('leave_date', '>=', Eval('termination_notice_date')),
               (),
            )
        ],
        depends=_depends + ['join_date', 'termination_notice_date'],
        states={
            'invisible': Eval('state') == 'draft',
            'required': ((Eval('state') == 'stopped')
                         | Eval('termination_notice_date')),
            'readonly': Eval('state') == 'stopped',
        },
        help="The date the member left the association.")
    memberships = fields.One2Many('association.membership', 'member',
        "Memberships",
        domain=[
            ('company', '=', Eval('company', -1))
            ],
        depends=_depends + ['company'],
        states={'readonly': Eval('state') == 'stopped'})
    memberships_names = fields.Function(
        fields.Char("Memberships"), 'get_memberships_names')

    del _depends

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            (
                'code_uniq',
                Exclude(t, (t.code, Equal), where=t.state != 'draft'),
                'association.msg_member_code_unique'
            ),
            (
                'party_uniq',
                Exclude(t, (t.party, Equal), where=t.state != 'stopped'),
                'association.msg_member_party_unique'
            ),
        ]

        cls._transitions |= {('draft', 'admitted'),
                             ('admitted', 'running'),
                             ('admitted', 'stopped'),
                             ('running', 'stopped')}

        cls._buttons.update({
            'admit': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
            },
            'run': {
                'pre_validate': [
                    ('join_date', '!=', None),
                    ],
                'invisible': Eval('state') != 'admitted',
                'depends': ['state'],
            },
            'stop': {
                'pre_validate': [
                    ('leave_date', '!=', None),
                    ],
                'invisible': Eval('state').in_(['draft', 'stopped']),
                'depends': ['state'],
            },
        })

    def get_code_readonly(self, name):
        return True

    @classmethod
    def _new_code(cls, **pattern):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Configuration = pool.get('association.configuration')
        config = Configuration(1)
        sequence = config.get_multivalue('member_sequence', **pattern)
        if sequence:
            return Sequence.get_id(sequence.id)

    def get_rec_name(self, name):
        return self.party.rec_name

    def get_memberships_names(self, name):
        if self.state == 'stopped':
            return ''
        today = Pool().get('ir.date').today()
        names = (ms.membership_type.name
                 for ms in self.memberships
                 if (ms.start_date and ms.start_date <= today
                     and (not ms.end_date or ms.end_date >= today)))
        names = ', '.join(names)
        if self.state == 'draft':
            names = f'({names})'
        return names

    def get_termination_notice_period(self, name):
        return self._get_termination_notice_period()

    @staticmethod
    def _get_termination_notice_period():
        pool = Pool()
        Configuration = pool.get('association.configuration')
        config = Configuration(1)
        return config.termination_notice_period

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        code_value = clause[2]
        if clause[1].endswith('like'):
            code_value = lstrip_wildcard(clause[2])
        return [
            bool_op,
            ('code', clause[1], code_value) + tuple(clause[3:]),
            ('party', ) + tuple(clause[1:]),
        ]

    @classmethod
    def copy(cls, members, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('code', None)
        default.setdefault('termination_notice_date', None)
        default.setdefault('leave_date', None)
        return super().copy(members, default=default)

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_code_readonly(cls, **pattern):
        Configuration = Pool().get('association.configuration')
        config = Configuration(1)
        return bool(config.get_multivalue('member_sequence', **pattern))

    @classmethod
    def validate(cls, members):
        super().validate(members)
        period = cls._get_termination_notice_period()
        if not period:
            # No termination_notice_period, thus nothing to validate
            return
        Warning = Pool().get('res.user.warning')

        try:
            Warning.format
        except AttributeError:
            # backward-compatibility for Tryton < 6.0
            def format_warning(name, records):
                key = '|'.join(map(str, records)).encode('utf-8')
                return '%s.%s' % (hashlib.md5(key).hexdigest(), name)
            Warning.format = format_warning

        for member in members:
            if member.state == 'draft':
                continue
            # Validate termination_notice_period is observed
            if member.termination_notice_date and member.leave_date \
               and member.termination_notice_date + period > member.leave_date:
                key = Warning.format(
                    'association.notice_period_not_observed',
                    (member, member.termination_notice_date,
                     member.leave_date))
                if Warning.check(key):
                    raise NoticePeriodNotObservedWarning(key,
                        gettext('association.msg_notice_period_not_observed',
                                member=member.party.name))

    @classmethod
    @ModelView.button
    @Workflow.transition('admitted')
    def admit(cls, members):
        for member in members:
            if not member.code:
                member.code = cls._new_code()
        cls.save(members)

    @classmethod
    @ModelView.button
    @Workflow.transition('running')
    def run(cls, members):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('stopped')
    def stop(cls, members):
        pool = Pool()
        Fee = pool.get('association.membership.fee')
        pending_fees = Fee.search([('member', 'in', members)])
        if not all(x.paid for x in pending_fees):
            raise MemberUnpaidFeeError(
                gettext('association.msg_unpaid_fee',
                        member=", ".join([x.rec_name for x in members])))


class MembersBookReport(Report):
    'Members Book report'
    __name__ = 'association.member_print'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Member = pool.get('association.member')
        Company = pool.get('company.company')
        clause = [('state', '!=', 'draft'), ('company', '=', data['company'])]
        context = super().get_context(records, data)
        members = Member.search(clause)
        context["members"] = members
        context["company"] = Company(data['company'])
        return context


class PrintMembersBookStart(ModelView):
    'Print Members Book'
    __name__ = 'association.member.print_member_book.start'
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class MembersBookWizard(Wizard):
    'Print member book'
    __name__ = 'association.member.print_member_book'
    start = StateView(
        'association.member.print_member_book.start',
        'association.print_member_book_start_view_from', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True)
        ])
    print_ = StateReport('association.member_print')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
        }
        return action, data


class MembershipType(ModelSQL, ModelView):
    """Membership Type"""
    __name__ = 'association.membership.type'

    name = fields.Char('Name', size=None, required=True)
    periods = fields.One2Many('association.membership.period',
        'membership_type',
        "Periods",
        help="The periods that form the basis of the membership type.")
    journal = fields.Many2One('account.journal', "Journal", required=True,
        domain=[('type', '=', 'revenue')],
        ondelete='RESTRICT',
        help="The accounting journal that is used for the fee.")
    account_revenue = fields.Many2One('account.account', "Account Revenue",
        domain=[
            ('type.revenue', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        required=True,
        ondelete='RESTRICT',
        help="The revenue account that is used for the fee.")
    maturity_delay = fields.TimeDelta("Maturity Delay",
        required=True,
        help="The period of time after the membership fees must be paid")
    company = fields.Many2One('company.company', 'Company', required=True,
        select=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
            ],
        ondelete='RESTRICT')

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_maturity_delay(cls):
        return timedelta(0)


class Membership(ModelSQL, ModelView):
    """Membership"""
    __name__ = 'association.membership'

    _states = {'readonly': Eval('member_state') == 'stopped'}
    _depends = ['member_state']

    member = fields.Many2One('association.member', "Member",
        states=_states,
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1))
            ],
        depends=_depends + ['company'],
        required=True,
        ondelete='CASCADE')
    membership_type = fields.Many2One('association.membership.type',
        "Membership Type",
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1))
            ],
        depends=_depends + ['company'],
        states=_states,
        ondelete='RESTRICT')
    company = fields.Many2One('company.company', "Company", required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], select=True, states=_states, depends=_depends)
    start_date = fields.Date("Start Date",
        domain=[
            If(
                And((Eval('start_date')),
                    (Eval('end_date'))),
                ('start_date', '<=', Eval('end_date')),
                (),
            )],
        depends=_depends + ['end_date'],
        states=_states,
        help="The date when the membership starts.")
    end_date = fields.Date("End Date",
        domain=[
            If(
                And(Eval('start_date'), Eval('end_date')),
                ('end_date', '>=', Eval('start_date')),
                (),
            )],
        depends=_depends + ['start_date'],
        states=_states,
        help="The date when the membership ends.")
    member_state = fields.Function(fields.Selection(_MEMBER_STATES,
        "Member State"), 'on_change_with_member_state')
    member_join_date = fields.Function(fields.Date("Member Join Date"),
        'on_change_with_member_join_date')
    member_leave_date = fields.Function(fields.Date("Member Leave Date"),
        'on_change_with_member_leave_date')

    del _states, _depends

    @classmethod
    def default_member_state(cls):
        return 'draft'

    @classmethod
    def validate(cls, memberships):
        super().validate(memberships)
        for membership in memberships:
            membership.check_dates()

    def check_dates(self):
        transaction = Transaction()
        connection = transaction.connection
        transaction.database.lock(connection, self._table)
        table = self.__table__()
        cursor = connection.cursor()
        cursor.execute(*table.select(table.id,
            where=(((table.start_date <= self.start_date)
                & (table.end_date >= self.start_date))
                | ((table.start_date <= self.end_date)
                & (table.end_date >= self.end_date))
                | ((table.start_date >= self.start_date)
                & (table.end_date <= self.end_date)))
            & (table.membership_type == self.membership_type.id)
            & (table.id != self.id)))
        period_id = cursor.fetchone()
        if period_id:
            overlapping_period = self.__class__(period_id[0])
            raise PeriodDateOverlapError(
                gettext('association.msg_overlapping_membership',
                    first=self.rec_name,
                    second=overlapping_period.rec_name))

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @fields.depends('member', '_parent_member.state')
    def on_change_with_member_state(self, name=None):
        if self.member:
            return self.member.state
        else:
            return 'draft'

    @fields.depends('member', '_parent_member.join_date')
    def on_change_with_member_join_date(self, name=None):
        if self.member:
            return self.member.join_date

    @fields.depends('member', '_parent_member.leave_date')
    def on_change_with_member_leave_date(self, name=None):
        if self.member:
            return self.member.leave_date


class Period(ModelSQL, ModelView):
    """Membership period"""
    __name__ = 'association.membership.period'

    membership_type = fields.Many2One('association.membership.type',
        "Membership Type",
        required=True,
        select=True,
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1))
            ],
        depends=['company'],
        ondelete='RESTRICT')
    company = fields.Many2One('company.company', "Company", required=True,
        select=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
            ],
        ondelete='RESTRICT')
    name = fields.Char("Name", size=None, required=True)
    start_date = fields.Date("Starting Date", required=True,
        domain=[
            ('start_date', '<=',
            Eval('end_date', None))
            ],
        depends=['end_date'])
    end_date = fields.Date("Ending Date", required=True,
        domain=[('end_date', '>=', Eval('start_date', None))],
        depends=['start_date'])
    amount = fields.Numeric("Amount", fee_digit, required=True,
        help="The fee amount.")

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def validate(cls, periods):
        super().validate(periods)
        for period in periods:
            period.check_dates()
            period.check_fee()

    def check_fee(self):
        if self.amount < Decimal('0.0'):
            raise AmountLessThanZeroError(
                gettext('association.msg_period_fee_invalid',
                period=self.rec_name))

    def check_dates(self):
        transaction = Transaction()
        connection = transaction.connection
        transaction.database.lock(connection, self._table)
        table = self.__table__()
        cursor = connection.cursor()
        cursor.execute(*table.select(table.id,
            where=(((table.start_date <= self.start_date)
                & (table.end_date >= self.start_date))
                | ((table.start_date <= self.end_date)
                & (table.end_date >= self.end_date))
                | ((table.start_date >= self.start_date)
                & (table.end_date <= self.end_date)))
            & (table.membership_type == self.membership_type.id)
            & (table.id != self.id)))
        period_id = cursor.fetchone()
        if period_id:
            overlapping_period = self.__class__(period_id[0])
            raise PeriodDateOverlapError(
                gettext('association.msg_overlapping_period',
                        first=self.rec_name,
                        second=overlapping_period.rec_name))


class Fee(ModelSQL, ModelView):
    """Membership Fee"""
    __name__ = 'association.membership.fee'

    _states = {
        'readonly':
            Bool(Eval('move'))
            and Eval('fee', Decimal('0.0')) == Decimal('0.0')
    }

    member = fields.Many2One('association.member', "Member", required=True,
        domain=[('state', '=', 'running')],
        states=_states,
        ondelete='RESTRICT')
    period = fields.Many2One('association.membership.period', "Period",
        required=True,
        ondelete='RESTRICT',
        states=_states,
        help="The period associated to this fee.")
    move = fields.Many2One('account.move', "Move", ondelete='RESTRICT',
        states=_states,
        help="The accounting move associated to this fee.")
    company = fields.Many2One('company.company', "Company", required=True,
        select=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
            ],
        ondelete='RESTRICT',
        states=_states)
    move_state = fields.Function(fields.Selection(_MOVE_STATES,
        "Move State"), 'on_change_with_move_state')
    paid = fields.Function(fields.Boolean('paid'), 'is_paid')
    reconciled = fields.Function(fields.Date("Reconciled"), 'get_reconciled')
    amount = fields.Function(fields.Numeric("Amount", fee_digit),
        'on_change_with_amount', searcher='search_amount')

    del _states

    @classmethod
    def __setup__(cls):
        super().__setup__()
        table = cls.__table__()

        cls._sql_constraints = [
            (
                'code_uniq',
                Unique(table, table.member, table.period),
                'association.msg_fee_member_period_unique'
            ),
        ]

        cls._buttons.update({
            'post_move': {
                'invisible':
                    (Eval('move_state') == 'posted')
                    | (Eval('amount', Decimal('0.0')) == Decimal('0.0')),
                'depends': ['move_state', 'amount'],
            },
        })

    @fields.depends('period', '_parent_period.amount')
    def on_change_with_amount(self, name=None):
        if self.period:
            return self.period.amount

    @fields.depends('move', '_parent_move.state')
    def on_change_with_move_state(self, name=None):
        if self.move:
            return self.move.state

    @classmethod
    def delete(cls, fees):
        for fee in fees:
            if fee.move or fee.paid:
                raise AccessError(
                    gettext('association.msg_fee_delete', fee=fee.rec_name))
        super().delete(fees)

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def search_amount(cls, name, clause):
        return [('period.amount',) + tuple(clause[1:])]

    def get_reconciled(self, name):
        def get_reconciliation(line):
            if line.reconciliation and line.reconciliation.delegate_to:
                return get_reconciliation(line.reconciliation.delegate_to)
            else:
                return line.reconciliation

        if not self.move:
            return None
        account = self.member.party.account_receivable
        receivable = []

        for line in self.move.lines:
            if line.account == account:
                receivable.append(line)

        reconciliations = list(map(get_reconciliation, receivable))
        if not reconciliations:
            return None
        elif not all(reconciliations):
            return None
        else:
            return max(r.date for r in reconciliations)

    def is_paid(self, name):
        return (self.reconciled is not None
            or self.move is None and self.period.amount == Decimal('0.0'))

    def get_move_line(self, amount, amount_second_currency):
        'Return counterpart Move Line for the amount'
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        if not self.member.party.account_receivable:
            raise PartyAccountPayableRequiredError(
                gettext('association.msg_party_receivable_missing',
                    party=self.member.party.rec_name))

        account = None
        if amount > 0:
            account = self.member.party.account_receivable
        else:
            account = self.period.membership_type.account_revenue

        party = None
        if account.party_required:
            party = self.member.party

        maturity_date = self.period.start_date + \
            self.period.membership_type.maturity_delay

        return MoveLine(
            debit=abs(amount) if amount > 0 else 0,
            credit=abs(amount) if amount < 0 else 0,
            account=account,
            maturity_date=maturity_date,
            party=party,
        )

    def create_move(self, date=None):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Date = pool.get('ir.date')

        move_date = date or Date.today()
        period_id = Period.find(self.company.id, date=move_date)
        move_lines = [
            self.get_move_line(self.period.amount, Decimal('0.0')),
            self.get_move_line(-self.period.amount, Decimal('0.0'))
        ]

        return Move(period=period_id,
            journal=self.period.membership_type.journal,
            date=move_date,
            origin=self,
            company=self.company,
            description=self.period.name,
            lines=move_lines)

    @classmethod
    @ModelView.button
    def post_move(cls, fees, date=None):
        pool = Pool()
        Date = pool.get('ir.date')
        Move = pool.get("account.move")
        to_post = []
        for fee in fees:
            if not fee.move and fee.period.amount > Decimal('0.0'):
                fee.move = fee.create_move(Date.today())
                fee.save()
                to_post.append(fee.move)

        Move.post(to_post)

    @classmethod
    def generate_fees(cls, date=None):
        pool = Pool()
        Date = pool.get('ir.date')
        MembershipLine = pool.get('association.membership')
        Fee = pool.get('association.membership.fee')
        Period = pool.get('association.membership.period')

        if not date:
            date = Date.today()

        def period_valid(line, d):
            period = Period.search([
                ('start_date', '<=', d),
                ('start_date', '<', line.end_date or line.member.leave_date
                 or d.max),
                ('start_date', '>=', line.start_date or line.member.join_date
                 or d.min),
                ('membership_type', '=', line.membership_type),
            ])
            return period

        active_lines = MembershipLine.search([('member.join_date', '<=', date),
            ('member.state', '=', 'running')])

        fees = []
        for line in active_lines:
            created_fee = Fee.search([('member', '=', line.member)])
            created_period = [x.period for x in created_fee]
            for period in period_valid(line, date):
                if period not in created_period:
                    fee = Fee(member=line.member,
                        period=period,
                        company=line.member.company)
                    fees.append(fee)
        cls.save(fees)


class PostFee(Wizard):
    'Post Fee'
    __name__ = 'association.membership.post_fee'
    start_state = 'post'
    post = StateTransition()

    def transition_post(self):
        pool = Pool()
        Fee = pool.get('association.membership.fee')
        fees = Fee.browse(Transaction().context['active_ids'])
        Fee.post_move(fees)
        return 'end'


class GenerateFee(Wizard):
    'Create Fees'
    __name__ = 'association.membership.fee_create'
    start = StateView(
        'association.membership.fee_create.start',
        'association.create_membership_fee_view_form', [
            Button("Cancel", 'end', 'tryton-cancel'),
            Button("Create", 'create_', 'tryton-ok', default=True),
        ])
    create_ = StateTransition()

    def transition_create_(self):
        pool = Pool()
        Fee = pool.get('association.membership.fee')
        Fee.generate_fees(date=self.start.date)
        return 'end'


class GenerateFeeStart(ModelView):
    "Create fees form"
    __name__ = 'association.membership.fee_create.start'

    date = fields.Date("Date", help="Generate fees up to this date.")

    @classmethod
    def default_date(cls):
        pool = Pool()
        date = pool.get('ir.date')
        return date.today()
