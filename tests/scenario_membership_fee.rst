==============
Membership Fee
==============

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax, create_tax_code
    >>> from trytond.modules.association.tests.tools import create_membership_type
    >>> from decimal import *
    >>> today = datetime.date.today()

Install association::

    >>> config = activate_modules('association')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> account_tax = accounts['tax']
    >>> account_cash = accounts['cash']

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.account_payable = payable
    >>> party.account_receivable = receivable
    >>> party.save()

Create member::

   >>> Member = Model.get('association.member')
   >>> member = Member()
   >>> member.party = party
   >>> member.save()

Get a revenue journal::

   >>> Journal = Model.get('account.journal')
   >>> journal_revenue, = Journal.find([
   ...         ('code', '=', 'REV'),
   ...         ])

Create membership types::

   >>> membership_type1 = create_membership_type(name="test1", company=company)
   >>> membership_type1.save()
   >>> membership_type2 = create_membership_type(name="test2", company=company)
   >>> membership_type2.save()

Add membership to member::

   >>> _ = member.memberships.new(
   ...   membership_type=membership_type1,
   ...   )
   >>> _ = member.memberships.new(
   ...   membership_type=membership_type2,
   ...   )
   >>> member.save()

Enroll member::

   >>> member.join_date =  today + relativedelta(month=1,day=1)
   >>> member.click('admit')
   >>> member.click('run')
   >>> member.save()

Create fee lines::

   >>> Fee = Model.get('association.membership.fee')
   >>> create_fee = Wizard('association.membership.fee_create')
   >>> create_fee.form.date = today + relativedelta(month=12)
   >>> create_fee.execute('create_')
   >>> len(Fee.find([])) > 0
   True

Post fee::

   >>> postedFee = Fee.find([('amount','>','0.0')])[0]
   >>> postedFee.click('post_move')
   >>> amount = postedFee.amount
   >>> receivable.reload()
   >>> amount == receivable.debit
   True
   >>> revenue.reload()
   >>> amount == revenue.credit
   True
