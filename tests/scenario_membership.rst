===================
Membership Scenario
===================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax, create_tax_code
    >>> from trytond.modules.association.tests.tools import create_membership_type
    >>> from decimal import *
    >>> today = datetime.date.today()

Install association::

    >>> config = activate_modules('association')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> account_tax = accounts['tax']
    >>> account_cash = accounts['cash']

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()

Create member::

   >>> Member = Model.get('association.member')
   >>> member = Member()
   >>> member.party = party
   >>> member.save()


Create membership type::

   >>> membership_type = create_membership_type(name="test", company=company)
   >>> membership_type.save()

Check overlapping date::

   >>> delta = relativedelta(weeks=4)
   >>> overlapping = membership_type.periods.new(
   ...     start_date=today,
   ...     end_date=today+delta,
   ...     name="overlapping",
   ...     amount=Decimal(42))
   >>> overlapping.save() #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
      ...
   PeriodDateOverlapError: ...

Check inverted date::

   >>> new_date = today + relativedelta(years=2)
   >>> inverted = membership_type.periods.new(
   ...     start_date=new_date + delta,
   ...     end_date=new_date,
   ...     name="inverted",
   ...     amount=Decimal(42))
   >>> inverted.save() #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
      ...
   DomainValidationError: ...

Check negative amount::

   >>> neg_type = create_membership_type(name="test", company=company)
   >>> negative = neg_type.periods.new(
   ...     start_date=new_date,
   ...     end_date=new_date + delta,
   ...     name="negative",
   ...     amount=Decimal(-10))
   >>> neg_type.save() #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
      ...
   AmountLessThanZeroError: ...

