# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest
import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown, doctest_checker


class AssociationTestCase(ModuleTestCase):
    'Test Association module'
    module = 'association'


def suite():
    suite = test_suite()
    suite.addTests(
        unittest.TestLoader().loadTestsFromTestCase(AssociationTestCase))
    for filename in (
            'scenario_membership.rst',
            'scenario_member.rst',
            'scenario_membership_fee.rst',
            'scenario_honorary_membership.rst',
            'scenario_member_change_date.rst',
            'regression_member.rst',
    ):
        suite.addTests(doctest.DocFileSuite(
            filename,
            tearDown=doctest_teardown,
            encoding='utf-8',
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE,
            checker=doctest_checker))
    return suite
