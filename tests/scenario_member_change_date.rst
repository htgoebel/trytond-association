==================
Member change date
==================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax, create_tax_code
    >>> from trytond.modules.association.tests.tools import create_membership_type
    >>> from decimal import *
    >>> today = datetime.date.today()

Install association::

    >>> config = activate_modules('association')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> account_tax = accounts['tax']
    >>> account_cash = accounts['cash']

Create parties::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.account_payable = payable
    >>> party.account_receivable = receivable
    >>> party.save()
    >>> party2 = Party(name='Party2')
    >>> party2.account_payable = payable
    >>> party2.account_receivable = receivable
    >>> party2.save()

Create members::

   >>> Member = Model.get('association.member')
   >>> member = Member()
   >>> member.party = party
   >>> member.save()
   >>> member2 = Member()
   >>> member2.party = party2
   >>> member2.save()

Create membership type::

   >>> membership_type = create_membership_type(name="test", company=company)
   >>> membership_type.save()


Add membership to member::

   >>> _ = member.memberships.new(membership_type=membership_type)
   >>> member.save()

Enroll member::

   >>> member.join_date = today + relativedelta(day=1,month=1)
   >>> member.save()
   >>> member.click('admit')
   >>> member.click('run')

Create fee lines::

   >>> Fee = Model.get('association.membership.fee')
   >>> create_fee = Wizard('association.membership.fee_create')
   >>> create_fee.form.date = today + relativedelta(months=3)
   >>> create_fee.execute('create_')
   >>> len(Fee.find([])) > 0
   True

Create fee after change date::

   >>> before_fees = set(Fee.find([]))
   >>> new_date = today + relativedelta(months=4)
   >>> member2.join_date = new_date
   >>> member2.save()
   >>> member2.click('admit')
   >>> member2.click('run')
   >>> create_fee = Wizard('association.membership.fee_create')
   >>> create_fee.form.date = today + relativedelta(month=6)
   >>> create_fee.execute('create_')
   >>> after_fees = set(Fee.find([]))
   >>> newfees = after_fees - before_fees
   >>> all(map(lambda x: x.period.start_date >= new_date, newfees))
   True
