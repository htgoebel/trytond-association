# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.association.tests.test_association import suite
except ImportError:
    from .test_association import suite

__all__ = ['suite']
