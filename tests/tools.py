# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import timedelta, date
from dateutil.relativedelta import relativedelta
from proteus import Model

from trytond.modules.company.tests.tools import get_company
from trytond.modules.account.tests.tools import get_accounts

__all__ = ['create_membership_type']


def date_interval(start_date, delta_time, quantity):
    first_date = start_date
    for period in range(quantity):
        last_date = first_date + delta_time
        yield (first_date, last_date)
        first_date = last_date + timedelta(days=1)


def create_membership_type(
        name,
        amount=Decimal(42),
        account=None,
        journal=None,
        today=None,
        company=None):
    MembershipType = Model.get('association.membership.type')
    Journal = Model.get('account.journal')

    if not company:
        company = get_company()

    if not today:
        today = date.today()

    if not journal:
        journal, = Journal.find([('code', '=', 'REV')], limit=1)

    if not account:
        account = get_accounts(company)['revenue']

    start_date = today + relativedelta(month=1, day=1)

    membership_type = MembershipType()
    membership_type.name = name
    membership_type.account_revenue = account
    membership_type.journal = journal

    for start, end in date_interval(start_date, relativedelta(months=1), 12):
        membership_type.periods.new(
            name=start.strftime("Test period %B"),
            start_date=start,
            end_date=end,
            amount=amount
        )

    return membership_type
