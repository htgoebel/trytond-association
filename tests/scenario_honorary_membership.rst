========================
Honorary member scenario
========================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax, create_tax_code
    >>> from trytond.modules.association.tests.tools import create_membership_type
    >>> from decimal import *
    >>> today = datetime.date.today()

Install association::

    >>> config = activate_modules('association')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> account_tax = accounts['tax']
    >>> account_cash = accounts['cash']

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.account_payable = payable
    >>> party.account_receivable = receivable
    >>> party.save()

Create member::

   >>> Member = Model.get('association.member')
   >>> member = Member()
   >>> member.party = party
   >>> member.save()


Create honorary membership::

   >>> honorary = create_membership_type(
   ...      name="honorary",
   ...      amount=Decimal(0.0),
   ...      company=company)
   >>> honorary.save()


Add membership to member::

   >>> _ = member.memberships.new(membership_type=honorary)
   >>> member.save()

Enroll member::

   >>> member.join_date = today
   >>> member.click('admit')
   >>> member.click('run')
   >>> member.save()

Create fee lines::

   >>> create_fee = Wizard('association.membership.fee_create')
   >>> create_fee.form.date = today + datetime.timedelta(weeks=40)
   >>> create_fee.execute('create_')

Check fees::

   >>> Fee = Model.get('association.membership.fee')
   >>> honorary_fees = Fee.find([('period.amount','=', Decimal('0.0'))])
   >>> all([x.paid for x in honorary_fees]) and len(honorary_fees) > 0
   True



