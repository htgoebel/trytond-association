============================
Regression Tests for Members
============================

Preperation
===============

Imports::

   >>> import datetime
   >>> from proteus import Model
   >>> from trytond.tests.tools import activate_modules
   >>> from trytond.modules.company.tests.tools import create_company, \
   ...     get_company
   >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
   ...     create_chart, get_accounts
   >>> today = datetime.date.today()

Install association::

    >>> config = activate_modules('association')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> account_cash = accounts['cash']

Create parties::

    >>> Party = Model.get('party.party')
    >>> party1 = Party(name='Party 1')
    >>> party1.account_receivable = receivable
    >>> party1.save()
    >>> party2 = Party(name='Party 2')
    >>> party2.account_receivable = receivable
    >>> party2.save()


Regression Tests
=================

When drafting a second member via GUI, the code of both members is set
to an empty string (since the GUI can not set it to ``None``). This
caused triggering the SQL constraints. Since proteus can handle
``None``, for triggering this case, ``code`` needs to be set to an
empty string explicitly.

Draft a first member::

    >>> Member = Model.get('association.member')
    >>> member1 = Member()
    >>> member1.party = party1
    >>> member1.code = ''  # simulate GUI
    >>> member1.save()
    >>> member1.reload()
    >>> member1.code
    ''

Draft a second member::

    >>> member2 = Member()
    >>> member2.party = party2
    >>> member2.code = ''  # simulate GUI
    >>> member2.save()
    >>> member2.reload()
    >>> member2.code
    ''


Test Constraints
====================

While in state `draft`, ``code`` is allowed to be non-unique::

    >>> member1.code = 'some-code'
    >>> member1.save()
    >>> member2.code = 'some-code'
    >>> member2.save()

Anyhow, ``code`` must be unique among all members not in state
`draft`.

  1. While both members have the same code, one is allowed to leave
     state `draft`::

       >>> member1.join_date = today
       >>> member1.click('admit')
       >>> member1.state
       'admitted'
       >>> member1.code
       'some-code'

  2. Anyhow, the second member must not change state, if the code is
     already taken by a non-`draft` member::

       >>> member2.code
       'some-code'
       >>> member2.join_date = today
       >>> member2.click('admit')  # doctest: +IGNORE_EXCEPTION_DETAIL
       Traceback (most recent call last):
            ...
       SQLConstraintError: ...
       >>> member2.state
       'draft'

  3. To allow changing state for the second member, change the
     ``code`` (here we set it to an empty string, like the GUI does,
     to make ``Member`` calculate a new value)::

       >>> member2.code = ''
       >>> member2.click('admit')
       >>> member2.state
       'admitted'
       >>> assert member1.code == 'some-code'
       >>> assert member2.code


The same ``code`` is not allowed after `draft` state.
(Actually one shouldn't be able to change the code at all after
`draft` state, but it's possible due to `issue 4207
<https://bugs.tryton.org/issue4207>`_.)::

    >>> member1.code
    'some-code'
    >>> member2.code = 'some-code'
    >>> member2.save()  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    SQLConstraintError: ...

..

   If the above code fails, check whether issue 4207 is fixed. If so,
   replace it by a test like this:

   Must not change ``code`` after `draft` state:

     >> member2.state
     'admitted'
     >> member2.code = 'some-other-code'  # simulate GUI
     >> member2.save() #doctest: +IGNORE_EXCEPTION_DETAIL
     Traceback (most recent call last):
          ...
     RequiredValidationError: ...
     >> member2.code = ''  # simulate GUI
     >> member2.save() #doctest: +IGNORE_EXCEPTION_DETAIL
     Traceback (most recent call last):
          ...
     RequiredValidationError: ...
     >> member2.code = None
     >> member2.save() #doctest: +IGNORE_EXCEPTION_DETAIL
     Traceback (most recent call last):
          ...
     RequiredValidationError: ...
