===============
Member Scenario
===============

Imports::

   >>> import datetime
   >>> from dateutil.relativedelta import relativedelta
   >>> from decimal import Decimal
   >>> from operator import attrgetter
   >>> from proteus import Model, Wizard
   >>> from trytond.tests.tools import activate_modules
   >>> from trytond.modules.company.tests.tools import create_company, \
   ...     get_company
   >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
   ...     create_chart, get_accounts
   >>> from trytond.modules.association.tests.tools import \
   ...      create_membership_type
   >>> from decimal import *
   >>> today = datetime.date.today()
   >>> timedelta = relativedelta(months=1)

Install association::

    >>> config = activate_modules('association')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> account_cash = accounts['cash']

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.account_receivable = receivable
    >>> party.save()

Create membership type::

   >>> membership_type = create_membership_type(name="test", company=company)
   >>> membership_type.save()

Create member::

   >>> Member = Model.get('association.member')
   >>> member = Member()
   >>> member.party = party
   >>> membership_line = member.memberships.new(membership_type=membership_type)
   >>> member.save()

Test membership inverse date::

   >>> membership_line = member.memberships.new(
   ... membership_type=membership_type,
   ... start_date=today + timedelta,
   ... end_date=today)
   >>> member.save() #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
        ...
   DomainValidationError: ...
   >>> member.memberships.remove(membership_line)
   >>> member.save()

Test memebrship fee::

   >>> Period = Model.get('association.membership.period')
   >>> Fee = Model.get('association.membership.fee')
   >>> period = Period.find([])[0]
   >>> fee = Fee(member=member,period=period)
   >>> fee.save() #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
        ...
   DomainValidationError: ...

Admitting requires Join Date::

   >>> member.click('admit') #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
        ...
   RequiredValidationError: ...
   >>> member.state
   'draft'

Make member active::

   >>> member.join_date = today
   >>> member.click('admit')
   >>> member.state
   'admitted'
   >>> member.click('run')
   >>> member.state
   'running'

Test memebrship fee move::

   >>> Period = Model.get('association.membership.period')
   >>> Fee = Model.get('association.membership.fee')
   >>> period = Period.find([])[0]
   >>> fee = Fee(member=member,period=period)
   >>> fee.save()
   >>> fee.click('post_move')
   >>> fee.move #doctest: +ELLIPSIS
   proteus.Model.get('account.move')(...)

Test member leave_date < join_date::

   >>> member.leave_date = today - timedelta
   >>> member.save() #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
      ...
   DomainValidationError: ...

Test member expulsion::

   >>> member.leave_date = today + timedelta
   >>> member.click('stop') #doctest: +IGNORE_EXCEPTION_DETAIL
   Traceback (most recent call last):
        ...
   MemberUnpaidFeeError: ...
   >>> move_wiz = Wizard('account.move.cancel',[fee.move])
   >>> move_wiz.form.description = 'Cancel'
   >>> move_wiz.execute('cancel')
   >>> fee.move.reload()
   >>> fee.paid
   True
   >>> member.click('stop')
   >>> member.state
   'stopped'


Test some constraints
==========================

`termination notice date` is given, `leave date`
----------------------------------------------------

Preparation::

    >>> party2 = Party(name='Party 2')
    >>> party2.save()
    >>> member2 = Member()
    >>> member2.party = party2
    >>> member2.save()
    >>> member2.join_date = today
    >>> member2.click('admit')
    >>> member2.click('run')

    >>> Configuration = Model.get('association.configuration')
    >>> config = Configuration(1)

If `termination notice date` is given, `leave date` must be given, too::

    >>> member2.termination_notice_date = today
    >>> member2.save()  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    RequiredValidationError: ...
    >>> member2.leave_date = today
    >>> member2.save()

If `termination notice period` is configured, and time between
`termination notice date` and `leave date` is to short, raise a
warning::

    >>> config.termination_notice_period = datetime.timedelta(days=90)
    >>> config.save()
    >>> member2.leave_date = today + timedelta
    >>> member2.save()  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    NoticePeriodNotObservedWarning: ...

When stopping a member, `leave date` must be set::

    >>> member2.termination_notice_date = None
    >>> member2.leave_date = None
    >>> member2.click('stop')  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    RequiredValidationError: ...

Same is true if `termination notice period` is not set::

    >>> config.termination_notice_period = None
    >>> config.save()
    >>> member2.click('stop') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    RequiredValidationError: ...


`party` must be unique
----------------------

Any party can be a member of the same association only once at a time.
The same party might apply for another membership later, of course.

Preparation::

    >>> party1 = Party(name='New Party 1')
    >>> party1.save()
    >>> member1 = Member()
    >>> member1.party = party1
    >>> member1.save()
    >>> member2 = Member()
    >>> member2.party = party1  # same party

No other member for same party allowed while member is in …

* `draft` state::

    >>> member2.save()  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    SQLConstraintError: ...

* `admitted` state::

    >>> member1.join_date = today
    >>> member1.click('admit')
    >>> member2.save()  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    SQLConstraintError: ...

* `running` state::

    >>> member1.click('run')
    >>> member2.save()  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
         ...
    SQLConstraintError: ...

Anyhow, another member for same party is allowed, if member has
resigned::

    >>> member1.leave_date = today
    >>> member1.click('stop')
    >>> member2.save()
