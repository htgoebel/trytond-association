# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model.exceptions import ValidationError
from trytond.exceptions import UserWarning


class PeriodValidationError(ValidationError):
    pass


class PeriodDateOverlapError(PeriodValidationError):
    pass


class DateNotInPeriodError(PeriodValidationError):
    pass


class SequenceMissingError(ValidationError):
    pass


class MemberUnpaidFeeError(ValidationError):
    pass


class AmountLessThanZeroError(ValidationError):
    pass


class PartyAccountPayableRequiredError(ValidationError):
    pass


class NoticePeriodNotObservedWarning(UserWarning):
    pass
